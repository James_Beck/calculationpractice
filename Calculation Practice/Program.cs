﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculation_Practice
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 10;
            var b = 12;
            var Sumab = a + b;

            ///My first choice of Console.WriteLine was decided because of it's simplicity
            Console.WriteLine($"The sum of variable (a) and variable (b) equals {Sumab}");
            ///My second choice of Console.WriteLine was decided because it allows changes to the variables 'a' and 'b' which lead to a more dynamic program
            Console.WriteLine($"The sum of {a} and {b} equals {Sumab}");
            
        }
    }
}
